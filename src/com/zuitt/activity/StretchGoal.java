package com.zuitt.activity;
import java.util.Scanner;

public class StretchGoal {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Input any character or symbol:");

        String text = input.nextLine();

        Scanner inputNumber = new Scanner(System.in);
        System.out.println("Input number of iterations:");

        int counter = inputNumber.nextInt();

        for (int i = 0; i <= counter; i++){
            for(int j = 0; j < i; j++){
                System.out.print(text + " ");
            }
            System.out.println();
        }
    }
}
