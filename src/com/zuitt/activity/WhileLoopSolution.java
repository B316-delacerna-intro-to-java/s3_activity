package com.zuitt.activity;
import java.util.Scanner;

public class WhileLoopSolution {
    public static void main(String[] args) {

        int counter = 1;
        int answer = 1;

        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");

        int num = in.nextInt();
        if (num > 0){
            try{
                while (counter <= num){
                    answer = answer * counter;
                    counter++;
                }

                System.out.println("The factorial of " + num + " is " + answer);

            } catch (Exception e){
                System.out.println("Invalid input");
                e.printStackTrace();
            }
        } else {
            System.out.println("Input positive numbers only!");
        }


    }
}
